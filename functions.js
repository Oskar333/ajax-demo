var data = [{
  "first_name": "Jojo",
  "last_name": "Drinkhill",
  "email": "jdrinkhill0@mlb.com",
  "avatar": "https://robohash.org/quiaetsunt.png?size=50x50&set=set1"
}, {
  "first_name": "Daveen",
  "last_name": "Tinsley",
  "email": "dtinsley1@wix.com",
  "avatar": "https://robohash.org/voluptatemconsecteturimpedit.jpg?size=50x50&set=set1"
}, {
  "first_name": "Merry",
  "last_name": "Gaskins",
  "email": "mgaskins2@virginia.edu",
  "avatar": "https://robohash.org/quishicenim.png?size=50x50&set=set1"
}, {
  "first_name": "Ellen",
  "last_name": "Lammie",
  "email": "elammie3@dailymotion.com",
  "avatar": "https://robohash.org/perferendisplaceatlabore.jpg?size=50x50&set=set1"
}, {
  "first_name": "Oliy",
  "last_name": "Enright",
  "email": "oenright4@google.fr",
  "avatar": "https://robohash.org/explicaborepellendusdistinctio.jpg?size=50x50&set=set1"
}]
var url = "https://pixabay.com/api/?key=KEY&category=animals&image_type=photo";
$(document).ready(function(){
	console.log("Jquery ok");
	$("#btnGetAll").click(function(){
		printResults(data);
		
	});
	$("#btnGetPhoto").click(function(){
		$("#containerPhoto").html("");
		launchAjax();		
	})

	function launchAjax() {

		$.ajax({
		  data: null,
		  type: "GET",
		  dataType: "json",
		  url: url
		})
		.done(function( data, textStatus, jqXHR ) {
		  console.log("La solicitud se ha completado correctamente."); 
		  printPhotos(data);
		})
		.fail(function( jqXHR, textStatus, errorThrown ) {
		  console.log( "La solicitud a fallado: " + textStatus);
		})
		.always(function(){
		  console.log("Aquí quitamos el loading");
		});
	}


	function printResults(data){
		for(var i = 0; i < data.length; i++) {
			var user = data[i];
			printCard(i);
			printAvatar(user, i);
			printFirstName(user, i);
			printLastName(user, i);
			printEmail(user, i);
			
		}
	}

	function printCard(i) {
		$("<div/>", {
			id: "box_" + i,
			class: "card"
		}).appendTo("#container");
	}

	function printFirstName(user, i) {
		$("<p/>", {
			text: user.first_name
		}).appendTo("#box_" + i);
	}

	function printLastName(user, i) {
		$("<p/>", {
			text: user.last_name
		}).appendTo("#box_" + i);
	}

	function printEmail(user, i) {
		$("<p/>", {
			text: user.email
		}).appendTo("#box_" + i);
	}

	function printAvatar(user, i) {
		$("<img/>", {
			src: user.avatar
		}).appendTo("#box_" + i);
	}

	function printPhotos(data) {
		
		for(var i = 0; i < data.hits.length; i++) {
			$("<img/>", {
				src: data.hits[i].previewURL
			}).appendTo("#containerPhoto");	
		}
		
	}

});